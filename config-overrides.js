const path = require("path");
const { override, useEslintRc, fixBabelImports, addLessLoader } = require("customize-cra");

module.exports = override(
  useEslintRc(),
  fixBabelImports("antd", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true,
  }),
  addLessLoader({
    lessOptions: {
      javascriptEnabled: true,
      modifyVars: {
        hack: `true; @import "${path.resolve(
          "src/assets/styles/antd/index.less"
        )}";`,
      },
    },
  })
);
