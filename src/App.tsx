import React from "react";

import ThemePreviewer from "./pages/ThemePreviewer";

function App() {
  return <ThemePreviewer />;
}

export default App;
